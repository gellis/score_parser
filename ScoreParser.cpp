/*
Given a text file of names and scores (see below), write a program using
correct syntax in the language of your choice�to read the file and print
all of the students that are �A� students (score of 90 or higher). Please
note that you have 10 minutes to craft your answer.

Alice Summers, 89
Bob Brown, 99
Jim Divers, 70
Dave Jones, 55
Daren Mann, 90
John Harrison, 9
Andrew Rodgers, 91
Kim J Smith, 76

Output should be:
Bob Brown
Daren Mann
Andrew Rodgers
*/

#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

class Student
{
public:
	int GetScore()
	{
		return m_score;
	}

	void SetScore(int score)
	{
		m_score = score;
	}

	std::string GetName()
	{
		return m_name;
	}
	void SetName(const std::string name)
	{
		m_name = name;
	}

private:
	string	m_name;
	int		m_score;
};


class StudentList
{
public:
	StudentList(const string students)		
	{
		// Initialize grade list by clearing the vector
		m_gradeList.clear();

		PopulateGradeListFromFile(students);
	}
		
	// List all student with a score of 90
	void ListAGradeStudents()
	{
		vector<Student>::iterator it;

		for ( it = m_gradeList.begin(); it != m_gradeList.end(); it++)
		{
			if ((*it).GetScore() >= 90)
			{
				cout << (*it).GetName() << endl;
			}
		}
	}


	void AddStudentToList(std::string& str)
	{
		if (str == "")
			return;

		Student student;
		std::string		cell;
		std::stringstream	lineStream(str);
		
		//  Store the name and grade of the student 

		for (int i = 0; i < 2; i++)
		{
			std::getline(lineStream, cell, ',');

			if (i == 0)
			{
				student.SetName(cell);
			}
			else
			{
				int num;
				istringstream(cell) >> num;
				student.SetScore(num);
			}
		}

		// Store the student into the grade list
		m_gradeList.push_back(student);
	}

	void PopulateGradeListFromFile(std::string filename)
	{

		std::ifstream 		fileData;
		fileData.open(filename, std::ifstream::in);

		for (std::string line; std::getline(fileData, line);)
		{
			AddStudentToList(line);
		}

	}

private:
	std::vector<Student>		m_gradeList;
	
};

int main()
{
	StudentList students("test.txt");
	students.ListAGradeStudents();
	cin.get();
	return 0;
}